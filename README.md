![Build Status](https://gitlab.com/groupme-community/api-docs/badges/master/pipeline.svg)

---
## Overview

This is a community driven and mantained representation of the Groupme Developer Public API

[Community API Docs](https://groupme-community.gitlab.io/api-docs/ "Docs")

[Offical API Docs](dev.groupme.com "Docs")

## Purpose

The purpose of this project is to provide a central source of knowledge that is more current
than the offical docs. Our goal is to create a source of helpful information to offer to the community.


## Contributions

As a project whose goal was to be maintained by the community, contributions are welcome!

Please fork then make any changes that you think should be updated. Please make sure and test your findings for
accuracy before making changes i.e. verify that the response status code is infact 202 instead of 201 if you are 
updating it to 202.

For testing your changes, I recommend using Visual Studio Code with the plugin: [openapi-designer](https://marketplace.visualstudio.com/items?itemName=philosowaffle.openapi-designer "plugin")

## Contacts

[Public GroupMe API Development Chat](https://groupme.com/join_group/27317261/ibNNhx "GroupMe Chat")


[Public GroupMe API Development Google Group](https://groups.google.com/forum/#!forum/groupme-api-support "GroupMe Google Group")

